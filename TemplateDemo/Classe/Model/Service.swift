//
//  Service.swift
//  Pockero
//
//  Created by Fabien CHUNG on 16-04-11.
//  Copyright © 2016 Pockero. All rights reserved.
//

import Foundation

enum RewardType{
    
    case drink
    case food
    case tips
}

enum ServiceState {
    
    case waitForHelper
    case waitForCreatorValidation
    case inProgress
    case closed
    case cancelledByOwner
    case expired
    case deleted
}

class Service: NSObject {
    
    // MARK: Required Properties
    
    var creatorId:      String
    var creationDate:   Date
    var message:        String
    var rewardType:     RewardType
    var state:          ServiceState
    
    // MARK: Optional Properties
    
    var helperId:               String?
    var address:                String?
    var rewardValue:            Int?
    var startDate:              Date?
    var endDate:                Date?
    var creatorCommentOnClose:  String?
    var helperCommentOnClose:   String?
    
    init(creatorId: String,
         creationDate: Date,
         message: String,
         rewardType: RewardType,
         state: ServiceState,
         rewardValue: Int? = nil) {
        
        self.creatorId      = creatorId
        self.creationDate   = creationDate
        self.message        = message
        self.rewardType     = rewardType
        self.state          = state
    }
}
