// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

import Foundation
import UIKit
import TemplateDemo

// swiftlint:disable file_length
// swiftlint:disable line_length
// swiftlint:disable type_body_length

protocol StoryboardSceneType {
  static var storyboardName: String { get }
}

extension StoryboardSceneType {
  static func storyboard() -> UIStoryboard {
    return UIStoryboard(name: self.storyboardName, bundle: nil)
  }

  static func initialViewController() -> UIViewController {
    guard let vc = storyboard().instantiateInitialViewController() else {
      fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
    }
    return vc
  }
}

extension StoryboardSceneType where Self: RawRepresentable, Self.RawValue == String {
  func viewController() -> UIViewController {
    return Self.storyboard().instantiateViewController(withIdentifier: self.rawValue)
  }
  static func viewController(identifier: Self) -> UIViewController {
    return identifier.viewController()
  }
}

protocol StoryboardSegueType: RawRepresentable { }

extension UIViewController {
  func perform<S: StoryboardSegueType>(segue: S, sender: Any? = nil) where S.RawValue == String {
    performSegue(withIdentifier: segue.rawValue, sender: sender)
  }
}

struct StoryboardScene {
  enum CreateService: String, StoryboardSceneType {
    static let storyboardName = "CreateService"

    static func initialViewController() -> TemplateDemo.CreateServiceViewController {
      guard let vc = storyboard().instantiateInitialViewController() as? TemplateDemo.CreateServiceViewController else {
        fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
      }
      return vc
    }

    case createServiceViewControllerScene = "CreateServiceViewController"
    static func instantiateCreateServiceViewController() -> TemplateDemo.CreateServiceViewController {
      guard let vc = StoryboardScene.CreateService.createServiceViewControllerScene.viewController() as? TemplateDemo.CreateServiceViewController
      else {
        fatalError("ViewController 'CreateServiceViewController' is not of the expected class TemplateDemo.CreateServiceViewController.")
      }
      return vc
    }
  }
  enum LaunchScreen: StoryboardSceneType {
    static let storyboardName = "LaunchScreen"
  }
  enum ListService: String, StoryboardSceneType {
    static let storyboardName = "ListService"

    static func initialViewController() -> UINavigationController {
      guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
        fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
      }
      return vc
    }

    case listServiceViewControllerScene = "ListServiceViewController"
    static func instantiateListServiceViewController() -> TemplateDemo.ListServiceViewController {
      guard let vc = StoryboardScene.ListService.listServiceViewControllerScene.viewController() as? TemplateDemo.ListServiceViewController
      else {
        fatalError("ViewController 'ListServiceViewController' is not of the expected class TemplateDemo.ListServiceViewController.")
      }
      return vc
    }
  }
  enum Main: StoryboardSceneType {
    static let storyboardName = "Main"

    static func initialViewController() -> UINavigationController {
      guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
        fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
      }
      return vc
    }
  }
}

struct StoryboardSegue {
}
