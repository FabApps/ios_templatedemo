// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

#if os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIColor
  typealias Color = UIColor
#elseif os(OSX)
  import AppKit.NSColor
  typealias Color = NSColor
#endif

extension Color {
  convenience init(rgbaValue: UInt32) {
    let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
    let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
    let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
    let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0

    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
}

// swiftlint:disable file_length
// swiftlint:disable line_length

// swiftlint:disable type_body_length
enum ColorName {
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#00a6f4"></span>
  /// Alpha: 100% <br/> (0x00a6f4ff)
  case azure
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#c5422c"></span>
  /// Alpha: 100% <br/> (0xc5422cff)
  case brick
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#00c2f3"></span>
  /// Alpha: 100% <br/> (0x00c2f3ff)
  case brightSkyBlue
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#323334"></span>
  /// Alpha: 100% <br/> (0x323334ff)
  case darkGrey
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#0085ff"></span>
  /// Alpha: 100% <br/> (0x0085ffff)
  case deepSkyBlue
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#f5f6f9"></span>
  /// Alpha: 100% <br/> (0xf5f6f9ff)
  case lightGrey
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#909397"></span>
  /// Alpha: 100% <br/> (0x909397ff)
  case mediumGrey
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ff3070"></span>
  /// Alpha: 100% <br/> (0xff3070ff)
  case gradientLeft
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ff896d"></span>
  /// Alpha: 100% <br/> (0xff896dff)
  case gradientRight

  var rgbaValue: UInt32 {
    switch self {
    case .azure:
      return 0x00a6f4ff
    case .brick:
      return 0xc5422cff
    case .brightSkyBlue:
      return 0x00c2f3ff
    case .darkGrey:
      return 0x323334ff
    case .deepSkyBlue:
      return 0x0085ffff
    case .lightGrey:
      return 0xf5f6f9ff
    case .mediumGrey:
      return 0x909397ff
    case .gradientLeft:
      return 0xff3070ff
    case .gradientRight:
      return 0xff896dff
    }
  }

  var color: Color {
    return Color(named: self)
  }
}
// swiftlint:enable type_body_length

extension Color {
  convenience init(named name: ColorName) {
    self.init(rgbaValue: name.rgbaValue)
  }
}
