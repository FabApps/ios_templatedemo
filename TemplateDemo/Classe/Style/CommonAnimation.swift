//
//  CommonAnimation.swift
//  Koolicar
//
//  Created by Koolifab on 16-12-22.
//  Copyright © 2016 Koolicar. All rights reserved.
//

import UIKit

class CommonAnimation: NSObject {
    
    
    /// Move a view from a specific point to his original point
    ///
    /// - Parameters:
    ///   - view: view to move
    ///   - initialPosition: initial position to move from
    ///   - duration: duration of the animation
    ///   - delay: delay before begining animation
    ///   - completion: completion
    class func moveToOriginPosition (_ view: UIView, from initialPosition: CGPoint, duration: TimeInterval, delay: TimeInterval, completion: ((Bool) -> Void)? = nil) {
        
        let translation = CGAffineTransform(translationX: initialPosition.x, y: initialPosition.y)
        let scale = CGAffineTransform(scaleX: 1, y: 1)
        view.transform = translation.concatenating(scale)
        
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping:2, initialSpringVelocity: 0, options: [], animations: {
            
            let translation = CGAffineTransform(translationX: 0, y: 0)
            let scale = CGAffineTransform(scaleX: 1, y: 1)
            view.transform = translation.concatenating(scale)
            
        }, completion: completion)
    }
    
    /// Move a view from his original point to a specific point
    ///
    /// - Parameters:
    ///   - view: view to move
    ///   - finalPosition: final position to move to
    ///   - duration: duration of the animation
    ///   - delay: delay before begining animation
    ///   - completion: completion
    class func moveFromOriginPosition(_ view: UIView, to finalPosition: CGPoint, duration: TimeInterval, delay: TimeInterval, completion: ((Bool) -> Void)? = nil) {
        
        let translation = CGAffineTransform(translationX: 0, y: 0)
        let scale = CGAffineTransform(scaleX: 1, y: 1)
        view.transform = translation.concatenating(scale)
        
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: 2, initialSpringVelocity: 0, options: [], animations: {
            
            let translation = CGAffineTransform(translationX: finalPosition.x, y: finalPosition.y)
            let scale = CGAffineTransform(scaleX: 1, y: 1)
            view.transform = translation.concatenating(scale)
            
        }, completion: completion)
    }
    
    /// Animate alpha by using backgroundColor
    ///
    /// - Parameters:
    ///   - view: view to animate
    ///   - initialAlpha: initial alpha (current if nil)
    ///   - finalAlpha: final alpha
    ///   - duration: duration of the animation
    ///   - delay: delay before begining animation
    ///   - completion: completion
    class func animateAlpha(_ view:UIView, from initialAlpha: CGFloat? = nil, to finalAlpha: CGFloat, duration: TimeInterval, delay: TimeInterval, completion: ((Bool) -> Void)? = nil) {
        
        // don't init alpha if no initialAlpha provided (just use the current)
        if let initialAlpha = initialAlpha {
            view.backgroundColor = view.backgroundColor?.withAlphaComponent(initialAlpha)
        }
        
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.7, options: [], animations: {
            
            view.backgroundColor = view.backgroundColor?.withAlphaComponent(finalAlpha)
            
        }, completion: completion)
    }
    
    
    /// Scroll Scrollview to a specific position
    ///
    /// - Parameters:
    ///   - scrollView: scrollView to scroll
    ///   - positionToScroll: final position of scrollview
    class func scrollViewAnimation(_ scrollView: UIScrollView, positionToScroll:CGFloat) {
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 2, initialSpringVelocity: 3, options: [], animations: {
            
            scrollView.contentOffset.y = positionToScroll
            
        }, completion: nil)
    }
    
    
    /// Animate constraint by changing constant
    ///
    /// - Parameters:
    ///   - constraint: constraint to Animate
    ///   - finalConstant: final constant of constraint
    ///   - parentView: parentView where the constraint is apply
    ///   - duration: duration of the animation
    ///   - delay: delay before begining animation
    ///   - completion: completion
    class func scaleConstraintAnimation (_ constraint: NSLayoutConstraint,
                                         finalConstant: CGFloat,
                                         parentView: UIView,
                                         duration: TimeInterval,
                                         delay: TimeInterval,
                                         completion: ((Bool) -> Void)? = nil)  {
        
        constraint.constant = finalConstant
        
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.7, options: [], animations: {
            
            parentView.layoutIfNeeded()
            
        }, completion: completion)
    }
}
