//
//  CreateServiceModuleProtocol.swift
//  Pockero
//
//  Created by Koolifab on 17-01-24.
//  Copyright (c) 2017 Pockero. All rights reserved.
//
//  This file was generated by the BVIPER Pockero Xcode Templates.
//

import Foundation

// This is the main interface of the module create service
// Here are listed all the methods available for the module

// MARK: - CreateServiceModuleProtocol

protocol CreateServiceModuleProtocol: class {
    
    /// Create service action
    ///
    /// - Parameters:
    ///   - reward: Reward type selected by user
    ///   - message: Message set by user
    ///   - value: optional value if user select tips reward
    func createService(reward: RewardType, message: String, value: String?)
}
