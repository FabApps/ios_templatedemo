//
//  CreateServiceModuleUserInterface.swift
//  Pockero
//
//  Created by Koolifab on 17-01-24.
//  Copyright (c) 2017 Pockero. All rights reserved.
//
//  This file was generated by the BVIPER Pockero Xcode Templates.
//

import Foundation

// This is the exit interface of the module create service
// Here are listed all methods delegate by the module

// MARK: - CreateServiceModuleUserInterface

protocol CreateServiceModuleUserInterface: class {
    
    /// Display error to user
    ///
    /// - Parameter message: error message formatted
    func displayError(message: String)
}
