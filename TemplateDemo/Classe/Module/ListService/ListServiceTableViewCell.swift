//
//  ListServiceTableViewCell.swift
//  TemplateDemo
//
//  Created by Koolifab on 17-03-06.
//  Copyright © 2017 FCH. All rights reserved.
//

import UIKit

class ListServiceTableViewCell: UITableViewCell {

    // MARK: IBOutlet
    @IBOutlet weak var infosView:           UIView!
    @IBOutlet weak var nameLabel:           UILabel!
    @IBOutlet weak var rewardImageView:     UIImageView!
    @IBOutlet weak var messageLabel:        UILabel!
    @IBOutlet weak var profileImageView:    UIImageView!
    
    // MARK: Cycle life
    
    override func awakeFromNib() {
        super.awakeFromNib()
        awakeFromNibInitStyle()
    }
    
    // MARK: Public methods
    func initWithDisplayItem(_ displayItem: ListServiceDisplayData) {
        
        nameLabel.text          = displayItem.userName
        messageLabel.text       = displayItem.message
        rewardImageView.image   = displayItem.rewardIcon
        profileImageView.image  = displayItem.photoProfile
    }
}

// MARK: - Common style implementation

extension ListServiceTableViewCell: StyleProtocol {
    
    func awakeFromNibInitStyle() {
        
    }
}
