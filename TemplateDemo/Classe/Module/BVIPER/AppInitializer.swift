//
//  AppInitializer.swift
//  Pockero
//
//  Created by Koolifab on 16-12-03.
//  Copyright © 2016 Pockero. All rights reserved.
//

import UIKit

// Initialize BVIPER
// Instantiate the first module

class AppInitializer: NSObject {
    
    let rootWireframe: RootWireframe = RootWireframe()
    
    func installRootViewControllerIntoWindow(window: UIWindow) {

        rootWireframe.window = window
        
        let listServiceBuilder = ListServiceBuilder(rootWireframe: rootWireframe)
        listServiceBuilder.build()
    }
}
