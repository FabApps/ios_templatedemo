//
//  Builder.swift
//  Pockero
//
//  Created by Koolifab on 16-12-03.
//  Copyright © 2016 Pockero. All rights reserved.
//

import Foundation

protocol Builder: class {
    func build()
}
