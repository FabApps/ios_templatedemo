//
//  StyleVariationsProtocol.swift
//  Pockero
//
//  Created by Koolifab on 16-12-03.
//  Copyright © 2016 Pockero. All rights reserved.
//

import Foundation

protocol StyleProtocol {
    
    func initializeStyleVariations()
    
    func initializeCommonStyle()
    func initializeIPhone4Style()
    func initializeIPhone5Style()
    func initializeIPhone6Style()
    func initializeIPhone6PlusStyle()
    
}

extension StyleProtocol {
    
    func initializeStyleVariations() {
        initializeCommonStyle()
        switch DeviceHelper.model {
        case .Four:
            initializeIPhone4Style()
        case .Five:
            initializeIPhone5Style()
        case .Six:
            initializeIPhone6Style()
        case .SixPlus:
            initializeIPhone6PlusStyle()
        default: break
        }
    }
    
    func initializeCommonStyle() {}
    func initializeIPhone4Style() {}
    func initializeIPhone5Style() {}
    func initializeIPhone6Style() {}
    func initializeIPhone6PlusStyle() {}
    
}
