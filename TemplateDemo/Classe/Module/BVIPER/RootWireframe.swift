//
//  RootWireframe.swift
//  Pockero
//
//  Created by Koolifab on 16-12-03.
//  Copyright © 2016 Pockero. All rights reserved.
//

import UIKit

public class RootWireframe: NSObject {
    
    var window: UIWindow?
    
    func pushViewController(viewController: UIViewController) {
        
        if let window = window {
            if let navigationController = navigationController(fromWindow: window) {
                navigationController.pushViewController(viewController, animated: true)
            }
        }
    }
    
    func present(_ viewControllerToPresent: UIViewController, animated: Bool, completion: (() -> Void)? = nil) {
        
        if let window = window {
            if let navigationController = navigationController(fromWindow: window) {
                navigationController.present(viewControllerToPresent, animated: animated, completion: completion)
            }
        }
    }
    
    func popViewController(animated: Bool) {
        
        if let window = window {
            if let navigationController = navigationController(fromWindow: window) {
                navigationController.popViewController(animated: animated)
            }
        }
    }
    
    func showRootViewController(_ viewController: UIViewController) {
        if let window = window {
            if let navigationController = navigationController(fromWindow: window) {
                navigationController.viewControllers = [viewController]
            }
        }
    }
    
    fileprivate func navigationController(fromWindow window: UIWindow) -> UINavigationController? {
        if let navigationController = window.rootViewController?.presentedViewController as? UINavigationController {
            return navigationController
        } else if let navigationController = window.rootViewController as? UINavigationController {
            return navigationController
        }
        return nil
    }
}
