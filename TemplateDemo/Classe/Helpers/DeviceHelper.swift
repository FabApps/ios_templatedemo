//
//  DeviceHelper.swift
//  TemplateDemo
//
//  Created by Koolifab on 17-03-05.
//  Copyright © 2017 FCH. All rights reserved.
//

import UIKit

public class DeviceHelper: NSObject {
    
    /// Device model
    ///
    /// - Four:    iPhone 4, 4S
    /// - Five:    iPhone 5, 5S, SE
    /// - Six:     iPhone 6, 6S, 7
    /// - SixPlus: iPhone 6 plus, 6S plus, 7 plus
    /// - iPad:    all iPads
    /// - Unknown: Unknown device
    public enum ModelDevice {
        case Four
        case Five
        case Six
        case SixPlus
        case iPad
        case Unknown
    }
    
    /// Current device model
    static var model: ModelDevice{
        let screenSize = UIScreen.main.bounds.size
        
        switch screenSize.height {
        case 480: return .Four
        case 568: return .Five
        case 667: return .Six
        case 736: return .SixPlus
        case 768: return .iPad
        default:  return .Unknown
        }
    }
}
