//
//  CreateServiceInteractorTests.swift
//  TemplateDemo
//
//  Created by Koolifab on 17-03-07.
//  Copyright © 2017 FCH. All rights reserved.
//

import XCTest
import Nimble
@testable import TemplateDemo

class CreateServiceInteractorTests: XCTestCase {
    
    // MARK: Test properties
    
    var createServiceInteractor:    CreateServiceInteractor!
    var mockPresenter:              MockCreateServicePresenter!
    
    override func setUp() {
        super.setUp()
        
        createServiceInteractor             = CreateServiceInteractor()
        mockPresenter                       = MockCreateServicePresenter()
        createServiceInteractor.delegate    = mockPresenter
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_createService_success() {
        
        // Given
        let reward  = RewardType.tips
        let message = "This is a test message"
        let value   = "10"
        
        // When
        createServiceInteractor.createService(with: message, reward: reward, value: value)
        
        // Then
        expect(self.mockPresenter.interactorDidCreateServiceCalled) == true
        expect(resultServices).toNot(be(nil))
    }
    
}
