//
//  ListServiceInteractorTests.swift
//  TemplateDemo
//
//  Created by Koolifab on 17-03-12.
//  Copyright © 2017 FCH. All rights reserved.
//

import XCTest
import Nimble
@testable import TemplateDemo

class ListServiceInteractorTests: XCTestCase {
    
    // MARK: Test properties
    
    var listServiceInteractor:    ListServiceInteractor!
    var mockPresenter:            MockListServicePresenter!
    
    override func setUp() {
        super.setUp()
        
        listServiceInteractor             = ListServiceInteractor()
        mockPresenter                     = MockListServicePresenter()
        listServiceInteractor.delegate    = mockPresenter
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_getServices() {
        
        // When
        listServiceInteractor.getServices()
        
        // Then
        expect(self.mockPresenter.interactorDidReturnServicesCalled) == true
    }
}
