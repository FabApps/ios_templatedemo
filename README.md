Template demo with BVIPER pattern

For more information about BVIPER please checkout: https://medium.com/mobile-travel-technologies/architecting-mobile-apps-with-b-viper-modules-e94e277c8d68#.ln3hsf69u

Tests on Builder, Presenter and Interactor which are essential of this pattern